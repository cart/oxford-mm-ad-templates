# Oxford-MM Age-Dependent Templates (ADTs)

Git LFS is required when cloning this repository. Alternatively, it can be downloaded as a compressed folder (next to the 'Clone' button) without having to install Git LFS.
The generic template, the OMM-1, from 240 UKB individuals in the 50-55 year age range can be downloaded from https://git.fmrib.ox.ac.uk/cart/oxford-mm-templates. 

All templates, the OMM-1 and the ADTs, are also available from OSF with the following link/DOI https://doi.org/10.17605/OSF.IO/S9GE4.

# Oxford-MM-1 ADTs
![Oxford-MM-1 ADTs shown in steps of 5 years - rows from top to bottom show the T1, T2-FLAIR, and DTI volumes, V1 transparancy-modulated by FA overlayed on the T2-FLAIR and a zoomed-in part, the log-Jacobian determinant map showing the corresponding distortion with respect to the OMM-1](/Oxford-MM-1_ADTs/age-dependent_templates.jpg "Oxford-MM-1 ADTs shown in steps of 5 years - rows from top to bottom show the T1, T2-FLAIR, and DTI volumes, V1 transparancy-modulated by FA overlayed on the T2-FLAIR and a zoomed-in part, the log-Jacobian determinant map showing the corresponding distortion with respect to the OMM-1")*Oxford-MM-1 ADTs shown in steps of 5 years - rows from top to bottom show the T1, T2-FLAIR, and DTI volumes, V1 transparancy-modulated by FA overlayed on the T2-FLAIR and a zoomed-in part, the log-Jacobian determinant map showing the corresponding distortion with respect to the OMM-1*

37,330 UBK individuals from the entire UKB age range were affinely and nonlinearly registered to the OMM-1. These registrations provided a linear transformation and a deformation field between each individual and the OMM-1. Using these resulting warps, GP regression was applied to model the age-specific average morphological brain shape. We used a radial basis function kernel to calculate the covariance between individuals' ages, and log-marginal likelihood (LML) maximisation to estimate hyperparameters, including the noise and length scale. Using the final model, a template for each year of age in the 45-81 range was constructed by predicting the average warp, composing its inverse with each of the 240 warps and affine transformations from the OMM-1 template construction, and applying this composition to the corresponding OMM-1 individual's modalities in native space. Thus, only one interpolation was required to transform a subject from its native space to each new age-dependent template space, where they were then averaged.

More information about the pipeline and how to cite:<br>
Arthofer, C., Smith, S.M., Douaud, G., Bartsch, A., Andersson, J., Lange, F. Age-dependent multimodal MRI template construction from UK Biobank, (2022). 28th Annual Meeting of the Organisation for Human Brain Mapping (OHBM)

Arthofer, C., Smith, S.M., Douaud, G., Bartsch, A., Andersson, J., Lange, F. Assessing the impact of age-specific template selection on spatial normalization. (2023). 29th Annual Meeting of the Organisation for Human Brain Mapping (OHBM)

Each ADT folder contains the following files:
| File | Description |
|---|---|
| `OMM1-ADT-<age>_T1_head.nii.gz` | T1 whole-head volume
| `OMM1-ADT-<age>_T2_FLAIR_head.nii.gz` | T2-FLAIR whole-head volume
| `OMM1-ADT-<age>_DTI_tensor.nii.gz` | DTI volume
| `OMM1-ADT-<age>_T1_brain.nii.gz` | Brain-masked T1
| `OMM1-ADT-<age>_DTI_mask.nii.gz` | Brain mask for DTI volume to reduce impact of noise around edges of brain (can be used as --mask_ref_tensor input to MMORF)
| `OMM1-ADT-<age>_T1_brain_mask_average.nii.gz` | Average brain mask
| `OMM1-ADT-<age>_T1_brain_mask_weighted.nii.gz` | Weighted brain mask for registrations with whole-head images only; reduces regularisation in the brain compared to skull and surrounding regions (can be used as --mask_ref_scalar input to MMORF)
| `OMM1-ADT-<age>_from-OMM1_to-ADT_warp.nii.gz` | Deformation field from OMM-1 space to ADT space

Contact: christoph.arthofer@ndcn.ox.ac.uk

DOI 10.17605/OSF.IO/S9GE4

Copyright, 2023, University of Oxford. All rights reserved
