import numpy as np
from math import pi
import matplotlib.pyplot as plt
from numpy.random import default_rng
from scipy.linalg import cholesky, cho_solve, det, solve_triangular
from scipy.optimize import minimize
from matplotlib.colors import LogNorm
import pandas as pd
import h5py
from fsl.utils.fslsub import func_to_cmd
import fsl_sub
import nibabel as nib
import os
from fsl.wrappers.fnirt import invwarp, applywarp, convertwarp
from fsl.wrappers import fslmaths
from file_tree import FileTree
from numpy.random import default_rng
from scipy.spatial.distance import cdist
import shlex
import subprocess
import sys
from scipy import stats
from random import random
import math

os.environ['FSLSUB_CONF'] = '/path/to/software/packages/fsl_sub_slurm/fsl_sub.yml'

# GP squared exponential kernel
def rbf_kernel(a, b, sigma_f, ell):
    dists = cdist(a / ell, b / ell, metric="sqeuclidean")
    K = sigma_f**2 * np.exp(-0.5 * dists)

    return K

# Log-marginal-likelihood based optimization
def obj_func_lml(theta, X, Y, W):
    sigma_f = theta[0]
    ell = theta[1]
    sigma_n = theta[2]

    K = rbf_kernel(X, X, sigma_f, ell)
    K_ = K + sigma_n**2 * W

    # scipy implementation
    try:
        L = cholesky(K_, lower=True)  # Line 2
    except np.linalg.LinAlgError:
        return -np.inf

    alpha = cho_solve((L, True), Y)
    log_likelihood_dims = -0.5 * np.einsum("ik,ik->k", Y, alpha)
    log_likelihood_dims -= np.log(np.diag(L)).sum()
    log_likelihood_dims -= 0.5 * K.shape[0] * np.log(2 * np.pi)
    log_likelihood = log_likelihood_dims.sum(-1)

    return -log_likelihood

# Cross-validation based optimization
def obj_func_cv(theta, X, Y, W):
    sigma_f = theta[0]
    ell = theta[1]
    sigma_n = theta[2]

    K = rbf_kernel(X, X, sigma_f, ell)
    K_ = K + sigma_n**2 * W

    try:
        L = cholesky(K_, lower=True)
    except np.linalg.LinAlgError:
        return -np.inf

    q_N = cho_solve((L, True), Y)
    K_inv_diag = np.diag(cho_solve((L, True), np.identity(L.shape[1]))).reshape(-1,1)

    cv = np.sum((q_N / K_inv_diag)**2)

    return cv

# Geisser's predictive probability based optimization
def obj_func_gpp(theta, X, Y, W):
    sigma_f = theta[0]
    ell = theta[1]
    sigma_n = theta[2]

    K = rbf_kernel(X, X, sigma_f, ell)
    K_ = K + sigma_n**2 * W

    N = K_.shape[0]
    K_inverse_ = np.linalg.inv(K_)
    q_N = np.dot(K_inverse_, Y)
    K_inv_diag = np.diag(K_inverse_).reshape(-1,1)

    gpp = 1/(2*N) * np.sum(q_N**2/K_inv_diag)
    gpp -= 1/(2*N) * np.sum(np.log(K_inv_diag))
    gpp += 0.5 * np.log(2 * np.pi)

    return gpp

# Choice of optimization method
def optimise(init_theta, X, Y, W, optimisation = 'LML'):
    if optimisation == 'LML':
        obj_func = obj_func_lml
    elif optimisation == 'CV':
        obj_func = obj_func_cv
    elif optimisation == 'GPP':
        obj_func = obj_func_gpp

    opt_res = minimize(obj_func, init_theta, args=(X, Y, W), method='Nelder-Mead', bounds=((0, None), (0, None), (0, None)))
    [sigma_f, ell, sigma_n], func_min = opt_res.x, opt_res.fun
    return sigma_f, ell, sigma_n, func_min

# Prediction of a new deformation field based on determined hyperparameters
def predict(X, Y, W, x_test, theta):
    sigma_f = theta[0]
    ell = theta[1]
    sigma_n = theta[2]

    k_mml = rbf_kernel(X, x_test, sigma_f, ell)
    K = rbf_kernel(X, X, sigma_f, ell)
    K_ = K + sigma_n**2 * W

    try:
        L = cholesky(K_, lower=True)
    except np.linalg.LinAlgError:
        return -np.inf
    alpha = cho_solve((L, True), Y)

    Y_test = k_mml.T.dot(alpha)
    v = cho_solve((L, True), k_mml)
    y_cov = rbf_kernel(x_test, x_test, sigma_f, ell) - k_mml.T.dot(v)

    return Y_test, y_cov


def get_cmap(n, name='rainbow'):
    return plt.cm.get_cmap(name, n)

def submitJob(command, name, log_dir, queue, wait_for=None, array_task=False, coprocessor=None, coprocessor_class=None, coprocessor_multi="1", threads=1, export_var=None, jobram=None, project=None):
    coprocessor_class_strict = True if coprocessor_class is not None else False

    hold_ids = []
    if wait_for is not None:
        for job_id in wait_for:
            if len(job_id) > 0:
                hold_ids.append(job_id)

    job_id = fsl_sub.submit(command=command,
                   array_task=array_task,
                   jobhold=hold_ids,
                   name=name,
                   logdir=log_dir,
                   queue=queue,
                   coprocessor=coprocessor,
                   coprocessor_class=coprocessor_class,
                   coprocessor_class_strict=coprocessor_class_strict,
                   coprocessor_multi=coprocessor_multi,
                   threads=threads,
                   export_vars=export_var,
                   jobram=jobram,
                   project=project
                   )

    return str(job_id)

def runGPR_optimise(tree, X, Y_init, W, initial_theta, optimisation='LML'):
    mask_img = nib.load(tree.get('template_mask')).get_fdata()
    mask_1d = mask_img.ravel()
    mask_3d = np.stack([mask_img for _ in range(3)], axis=-1)
    mask_3d = mask_3d.ravel()

    mask_sampler = np.arange(0,len(mask_1d))
    mask_bin = np.zeros_like(mask_sampler)
    mask_sampler = mask_sampler[mask_1d > 0.5]
    n_samples = 10000
    rng = default_rng(42)
    mask_sample = rng.choice(mask_sampler, n_samples, replace=False)
    mask_bin[mask_sample] = 1
    mask_bin = np.hstack([mask_bin for _ in range(3)])
    mask_sampler = np.arange(0,len(mask_3d))
    mask_sample = mask_sampler[mask_bin > 0]
    mask_sample = np.sort(mask_sample)

    Y = Y_init[:, mask_sample]
    print(Y.shape)

    Y_var = np.var(Y,axis=0)
    Y_var = np.average(Y_var)
    print('Variance:', Y_var)

    Y_sd = np.std(Y,axis=0)
    Y_sd = np.average(Y_sd)
    print('SD: ', Y_sd)

    Y_sem = stats.sem(Y,axis=0)
    Y_sem = np.average(Y_sem)
    print('SEM: ', Y_sem)

    sigma_f, ell, sigma_n, func_min = optimise(initial_theta, X, Y, W, optimisation)

    print(optimisation, initial_theta, sigma_f, ell, sigma_n, func_min)

    theta_optimised = np.array([sigma_f, ell, sigma_n, func_min])
    tree = tree.update(opt_method=optimisation)
    np.savez_compressed(tree.get('theta_optimised'), theta_optimised=theta_optimised)
    np.savetxt(tree.get('theta_optimised_csv'), theta_optimised, delimiter=',', fmt='%f')

def runGPR_predict(tree, X, W, X_test, Y):
    theta_data = np.load(tree.get('theta_optimised'))
    theta_optimised = theta_data['theta_optimised']

    f = h5py.File(tree.get('y_test'),'w')
    Y_test = f.create_dataset('y_test', (len(X_test),Y.shape[1]), 'f')
    y_cov = f.create_dataset('y_cov_test', (len(X_test),1), 'f')
    i = 0
    n_splits = 1
    n = Y.shape[1] / n_splits
    while i < n_splits:
        print(i)
        Y_test[:,int(i*n):int((i+1)*n)], y_cov_temp = predict(X, Y[:,int(i*n):int((i+1)*n)], W, X_test, theta_optimised[0:3])
        y_cov[:,i] = np.sqrt(np.diag(y_cov_temp))
        i += 1

    print('Y_test shape: ', Y_test.shape)

    f.close()

def readData(tree, stratified=False):

    if not stratified:
        df_ids = pd.read_csv(tree.get('selected_ids'), header=None, names=['ID'])
        ids = df_ids['ID'].tolist()

        print(len(ids))
        for i, id in enumerate(ids):
            tree = tree.update(sub_id=id)
            p = tree.get('mmorf_warp')
            print(i, p, flush=True)
            warp_arr = nib.load(p).get_fdata().astype(np.float32)
            if i == 0:
                sy = np.zeros((len(ids), np.prod(warp_arr.shape)),dtype=np.float32)

            sy[i, ...] = warp_arr.ravel()
        s = None
        w = None
        sigma_n_2 = None
        sigma_f_2 = None

    if stratified:
        df_ids = pd.read_csv(tree.get('selected_ids'), header=None, names=['ID'])
        ids = df_ids['ID'].tolist()

        df_x = pd.read_csv(tree.get('selected_ages'), header=None, names=['age'])
        X = df_x['age'].to_numpy().reshape(-1, 1)

        strat_factor = 0.5
        min_age = np.floor(np.amin(X))
        X_strat = np.arange(min_age, 83, strat_factor).reshape(-1, 1)
        print('X_stratified: ', X_strat)

        print(len(ids))
        print(len(X))
        for i, id in enumerate(ids):
            tree = tree.update(sub_id=id)
            p = tree.get('mmorf_warp')
            warp_arr = nib.load(p).get_fdata().astype(np.float32)
            if i == 0:
                sy = np.zeros((2, len(X_strat), np.prod(warp_arr.shape)))
                syv = np.zeros((2, len(X_strat), np.prod(warp_arr.shape)))
                w = np.zeros((2, len(X_strat)))
                s = np.zeros((2, len(X_strat)))

            rbin = 0 if random() < 0.5 else 1
            bin = int(np.ceil((X[i] - min_age) / strat_factor))

            print(i, ' Age: ', X[i], ' bin: ', bin, ' rbin: ', rbin, flush=True)

            sy[rbin,bin, ...] += warp_arr.ravel()
            w[rbin,bin] += 1
            s[rbin,bin] += X[i]
            syv[rbin,bin, ...] += warp_arr.ravel()**2

        sy = np.reshape(sy, (2*len(X_strat),-1))
        w = np.reshape(w, (2*len(X_strat),1))
        s = np.reshape(s, (2*len(X_strat),1))
        syv = np.reshape(syv, (2*len(X_strat),-1))

        notempty_index = w!=0
        if np.sum(~notempty_index) > 0:
            sy = sy[notempty_index.ravel(),:]
            w = w[notempty_index.ravel(),:]
            s = s[notempty_index.ravel(),:]
            syv = syv[notempty_index.ravel(),:]

        s = s / w

        notempty_index = w>1
        notempty_index = notempty_index.ravel()
        syv = (syv[notempty_index,:] - sy[notempty_index,:]**2 / w[notempty_index,:]) / (w[notempty_index,:] - 1)
        sigma_n_2 = np.sum((w[notempty_index,:] - 1) * syv, axis=0) / np.sum(w[notempty_index,:] - 1, axis=0)
        sigma_n_2 = np.average(sigma_n_2)

        sy = sy / w

        w_temp = w / np.sum(w)
        sigma_f_2 = np.average(np.sum(np.abs(sy - np.sum(sy * w_temp, axis=0))**2 * w_temp, axis=0))

        w = 1 / w

        print('initial estimate sigma_f_2: ', sigma_f_2)
        print('initial estimate sigma_n_2: ', sigma_n_2)

    return sy, s, w, sigma_n_2, sigma_f_2


def invertWarp(tree,  warp_info, warp_shape, idx):
    print(tree.get('pred_warp_img'))
    print(tree.get('y_test'))

    f = h5py.File(tree.get('y_test'), 'r')
    Y_test = f['y_test']

    img_nib = nib.Nifti1Image(Y_test[idx, :].reshape(warp_shape), affine=warp_info.affine, header=warp_info.header)
    img_nib.to_filename(tree.get('pred_warp_img'))

    invwarp(warp=tree.get('pred_warp_img'), ref=tree.get('template'), out=tree.get('pred_invwarp_img'))

def applyPredictedWarpT1(tree):
    convertwarp(out=tree.get('pred_warp_full_T1'), ref=tree.get('template'), premat=tree.get('T1_head_to_MNI_mat'), warp1=tree.get('template_warp'), warp2=tree.get('pred_invwarp_img'))
    applywarp(src=tree.get('T1_head_unbiased'), ref=tree.get('template'), out=tree.get('warped_T1'), warp=tree.get('pred_warp_full_T1'), interp='spline')
    convertwarp(out=tree.get('pred_warp_full_T1_brain'), ref=tree.get('template'), premat=tree.get('T1_brain_to_MNI_mat'), warp1=tree.get('template_warp'), warp2=tree.get('pred_invwarp_img'))
    applywarp(src=tree.get('T1_brain_mask'), ref=tree.get('template'), out=tree.get('warped_T1_brain_mask'), warp=tree.get('pred_warp_full_T1_brain'), interp='trilinear')

def applyPredictedWarpT2(tree):
    convertwarp(out=tree.get('pred_warp_full_T2'), ref=tree.get('template'), premat=tree.get('T2_head_to_MNI_mat'), warp1=tree.get('template_warp'), warp2=tree.get('pred_invwarp_img'))
    applywarp(src=tree.get('T2_head_unbiased'), ref=tree.get('template'), out=tree.get('warped_T2'), warp=tree.get('pred_warp_full_T2'), interp='spline')

def applyPredictedWarpDTI(tree):
    if not os.path.exists(tree.get('warped_DTI')):
        convertwarp(out=tree.get('pred_warp_full_DTI'), ref=tree.get('template'), premat=tree.get('DTI_to_MNI_mat'), warp1=tree.get('template_warp'), warp2=tree.get('pred_invwarp_img'))
        cmd = 'vecreg -i ' + tree.get('DTI_tensor') + \
              ' -r ' + tree.get('template') + \
              ' -o ' + tree.get('warped_DTI') + \
              ' -w ' + tree.get('pred_warp_full_DTI') + \
              ' --interp=spline \n'
        try:
            subprocess.run(shlex.split(cmd), capture_output=True, text=True, check=True)
        except subprocess.CalledProcessError as e:
            print(str(e), file=sys.stderr)

    applywarp(src=tree.get('DTI_mask_eroded'), ref=tree.get('template'), out=tree.get('warped_DTI_mask'), warp=tree.get('pred_warp_full_DTI'), interp='trilinear')

def averageImages(img_paths, out_path, mod='mean', norm_bool=False):
    n_imgs = len(img_paths)
    n_exist = 0
    if mod == 'mean':
        for i, img_path in enumerate(img_paths):
            if os.path.exists(img_path):
                n_exist += 1
                print(i, ' ', img_path)
                img_nib = nib.load(img_path)
                if norm_bool:
                    img_nib = fslmaths(img_nib).inm(1000).run()
                if i == 0:
                    sum_img = img_nib
                else:
                    sum_img = fslmaths(sum_img).add(img_nib).run()
            else:
                print(i, ' ', img_path, ' does not exist!')

        if n_exist > 0:
            mean_img = fslmaths(sum_img).div(n_exist).run()
            mean_img.to_filename(out_path)
    elif mod == 'median' or mod == 'mode':
        for i, img_path in enumerate(img_paths):
            if os.path.exists(img_path):
                n_exist += 1
                print(i, ' ', img_path)

        for i, img_path in enumerate(img_paths):
            if os.path.exists(img_path):
                img_nib = nib.load(img_path)
                if norm_bool:
                    img_nib = fslmaths(img_nib).inm(1000).run()
                if i == 0:
                    images = []

                images.append(img_nib.get_fdata())

        if n_exist > 0:
            if mod == 'median':
                m_img = np.median(np.array(images), axis=0)

            elif mod == 'mode':
                m_mode = stats.mode(np.array(images), axis=0)
                m_img = m_mode.mode
            m_nib = nib.Nifti1Image(np.squeeze(m_img), affine=img_nib.affine, header=img_nib.header)
            m_nib.to_filename(out_path)

    assert n_exist == n_imgs, "Not all images available!"

def normalize_template(template_path, WMmask_path, normalize_template_path):
    temp_nib = nib.load(template_path)
    temp_img = temp_nib.get_fdata()
    mask_img = nib.load(WMmask_path).get_fdata()

    mean_int = np.mean(temp_img[mask_img[:,:,:,21] > 0.99])
    new_img = temp_img / mean_int * 1000
    normalized_temp = nib.Nifti1Image(new_img, affine=temp_nib.affine, header=temp_nib.header)
    normalized_temp.to_filename(normalize_template_path)

if __name__ == "__main__":
    __file__ = './run_GPR_on_ukbb.py'
    tree_path = 'data_UKB_all.tree'
    warp_dir = './subjectsAll/'
    out_dir = './stratified_half-yearly/LML/'
    template_dir = './omm-1/'
    data_ud_dir = './uk-biobank/'

    tree = FileTree.read(tree_path, top_level='')
    tree = tree.update(output_dir=out_dir, template_dir=template_dir, warp_dir=warp_dir, data_ud_dir=data_ud_dir, id_dir=out_dir)

    os.makedirs(out_dir, mode=0o700) if not os.path.exists(out_dir) else print(out_dir + ' exists')
    script_dir = tree.get('script_dir')
    os.makedirs(script_dir, mode=0o700) if not os.path.exists(script_dir) else print(script_dir + ' exists')
    log_dir = tree.get('log_dir')
    os.makedirs(log_dir, mode=0o700) if not os.path.exists(log_dir) else print(log_dir + ' exists')

    cpuq = 'short'
    jobram_low = 128
    jobram_hi = 250000

    job_ids = ['' for _ in range(100)]
    tag = 'gpr'

    df_ids = pd.read_csv(tree.get('selected_ids'), header=None, names=['ID'])
    ids = df_ids['ID'].tolist()

    df_x = pd.read_csv(tree.get('selected_ages'), header=None, names=['age'])
    X = df_x['age'].to_numpy().reshape(-1,1)

    X_test_db = np.arange(45, 82).reshape(-1, 1)
    X_test = np.arange(45, 82).reshape(-1,1)
    n_X_test = len(X_test)

# Read in the data
    Y, X_, W_, init_sigma_n_2, init_sigma_f_2 = readData(tree=tree, stratified=True)
    X = X_

    if W_ is None:
        W = np.identity(X.shape[0])
    else:
        W = np.identity(X.shape[0]) * W_

# Run the optimisation to geet the hyperparameters
    task_name = 'runGPR_optimisation'
    optimisation = 'LML'
    init_sigma_f = np.sqrt(init_sigma_f_2)
    init_ell = 10
    init_sigma_n = np.sqrt(init_sigma_n_2)
    initial_theta = [init_sigma_f, init_ell, init_sigma_n]
    task_name = 'runGPR_optimisation'
    runGPR_optimise(tree, X, Y, W, initial_theta, optimisation)

    print(X.shape)

# Predict the deformation fields for specific ages
    tree = tree.update(opt_method=optimisation)
    task_name = 'runGPR_prediction'
    runGPR_predict(tree, X, W, X_test, Y)

    tree = tree.update(sub_id=ids[0])
    warp_info = nib.load(tree.get('mmorf_warp'))
    warp_shape = warp_info.get_fdata().shape

# Invert the predicted deformation fields
    task_name = 'invertWarps'
    script_path = os.path.join(script_dir, task_name + '.sh')
    with open(script_path, 'w') as f:
        for i, x_test in enumerate(X_test.flatten()):
            print(x_test, ' at ', i)
            tree = tree.update(x_test_val=x_test)
            os.mkdir(tree.get('x_test_dir')) if not os.path.exists(tree.get('x_test_dir')) else print(tree.get('x_test_dir'), ' already exists')
            jobcmd = func_to_cmd(invertWarp,
                                 args=(tree, warp_info, warp_shape, i),
                                 tmp_dir=script_dir,
                                 kwargs=None,
                                 clean="never")
            jobcmd = jobcmd + '\n'
            f.write(jobcmd)
    job_ids[2] = submitJob(script_path, tag + '_' + task_name, log_dir, queue=cpuq, wait_for=[job_ids[1]], array_task=True)
    print('submitted: ' + task_name)


    df_ids = pd.read_csv(tree.get('template_ids'), sep=' ', index_col=False, dtype={'subject_ID': str})
    template_ids = df_ids['subject_ID'].tolist()
    print(len(template_ids))

# Apply the inverted predicted deformation fields to the initial 240 individuals
    task_name = 'applyPredWarps'
    script_path = os.path.join(script_dir, task_name + '.sh')
    with open(script_path, 'w') as f:
        for i, x_test in enumerate(X_test.flatten()):
            print(x_test)
            tree = tree.update(x_test_val=x_test)
            jobcmd = 'fnirtfileutils --in=' + tree.get('pred_warp_img') + ' --ref=' + tree.get('pred_template_img_T1') + ' --out=' + tree.get('pred_coef_img') + ' --outformat=spline --warpres=1; '
            jobcmd += 'fnirtfileutils --in=' + tree.get('pred_coef_img') + ' --ref=' + tree.get('pred_template_img_T1') + ' --jac=' + tree.get('pred_jac_img') + '\n'
            for j, id in enumerate(template_ids):
                print(x_test, j)
                tree = tree.update(temp_sub_id=id, sub_id=id)

                jobcmd += func_to_cmd(applyPredictedWarpT1,
                                     args=(tree,),
                                     tmp_dir=script_dir,
                                     kwargs=None,
                                     clean="always") + '\n'
                jobcmd += func_to_cmd(applyPredictedWarpT2,
                                     args=(tree,),
                                     tmp_dir=script_dir,
                                     kwargs=None,
                                     clean="always") + '\n'
                jobcmd += func_to_cmd(applyPredictedWarpDTI,
                                     args=(tree,),
                                     tmp_dir=script_dir,
                                     kwargs=None,
                                     clean="never") + '\n'

            jobcmd = func_to_cmd(normalize_template,
                                 args=(tree.get('pred_template_img_T1'), tree.get('warped_a2009s'), tree.get('pred_template_img_T1_normalized')),
                                 tmp_dir=script_dir,
                                 kwargs=None,
                                 clean='always') + '\n'
            jobcmd += func_to_cmd(normalize_template,
                                  args=(tree.get('pred_template_img_T2'), tree.get('warped_a2009s'), tree.get('pred_template_img_T2_normalized')),
                                  tmp_dir=script_dir,
                                  kwargs=None,
                                  clean='always') + '\n'

            fslmaths(tree.get('pred_template_img_T1_normalized')).mul(tree.get('pred_template_img_T1_brain_mask_thresh')).run(tree.get('pred_template_img_T1_brain_normalized'))
            f.write(jobcmd)

    job_ids[3] = submitJob(script_path, tag + '_' + task_name, log_dir, queue=cpuq, wait_for=[job_ids[2]], array_task=True)
    print('submitted: ' + task_name)

# Average each modality of the warped 240 individuals
    task_name = 'averageImages'
    script_path = os.path.join(script_dir, task_name + '.sh')
    with open(script_path, 'w') as f:
        for i, x_test in enumerate(X_test.flatten()):
            print(x_test)
            img_paths = []
            for j, id in enumerate(template_ids):
                tree = tree.update(x_test_val=x_test, temp_sub_id=id, sub_id=id)
                img_paths.append(tree.get('warped_T1'))
            temp_path = tree.get('pred_template_img_T1')

            jobcmd = func_to_cmd(averageImages, args=(img_paths, temp_path, 'mean', True), tmp_dir=script_dir, kwargs=None, clean="never") + '\n'

            img_paths = []
            for j, id in enumerate(template_ids):
                tree = tree.update(x_test_val=x_test, temp_sub_id=id, sub_id=id)
                img_paths.append(tree.get('warped_T2'))
            temp_path = tree.get('pred_template_img_T2')

            jobcmd += func_to_cmd(averageImages, args=(img_paths, temp_path, 'mean', True), tmp_dir=script_dir, kwargs=None, clean="never") + '\n'

            img_paths = []
            for j, id in enumerate(template_ids):
                tree = tree.update(x_test_val=x_test, temp_sub_id=id, sub_id=id)
                img_paths.append(tree.get('warped_T1_brain_mask'))
            temp_path = tree.get('pred_template_img_T1_brain_mask')

            jobcmd = func_to_cmd(averageImages, args=(img_paths, temp_path, 'mean', False), tmp_dir=script_dir, kwargs=None, clean="never") + '\n'

            img_paths = []
            for j, id in enumerate(template_ids):
                tree = tree.update(x_test_val=x_test, temp_sub_id=id, sub_id=id)
                img_paths.append(tree.get('warped_DTI_mask'))
            temp_path = tree.get('pred_template_img_DTI_mask')

            jobcmd += func_to_cmd(averageImages, args=(img_paths, temp_path, 'mean', False), tmp_dir=script_dir, kwargs=None, clean="never") + '\n'

            f.write(jobcmd)

    job_ids[4] = submitJob(script_path, tag + '_' + task_name, log_dir, queue=cpuq, wait_for=[job_ids[3]], array_task=True)
    print('submitted: ' + task_name)

    task_name = 'averageTensors'
    mmorf_path = './mmorf_executable/mmorf.sif'
    mmorf_exec_cmd = 'singularity exec ' + mmorf_path
    export_paths = []
    script_path = os.path.join(script_dir, task_name + '.sh')
    with open(script_path, 'w') as f:
        for i, x_test in enumerate(X_test.flatten()):
            print(x_test)
            jobcmd = mmorf_exec_cmd + ' tensor_average' + ' -i '
            for j, id in enumerate(template_ids):
                tree = tree.update(x_test_val=x_test, temp_sub_id=id, sub_id=id)
                jobcmd += tree.get('warped_DTI') + ' '
                export_paths.append(tree.get('warped_DTI'))
            jobcmd += '-o ' + tree.get('pred_template_img_DTI') + '\n'
            export_paths.append(tree.get('pred_template_img_DTI'))
            f.write(jobcmd)

    common_path = os.path.commonpath(export_paths)
    print(common_path)
    export_var_str = {'SINGULARITY_BIND': '"SINGULARITY_BIND=' + ','.join([common_path]) + '"'}

    job_ids[5] = submitJob(script_path, tag + '_' + task_name, log_dir, queue=cpuq, wait_for=[job_ids[3]], array_task=True, export_var=[export_var_str['SINGULARITY_BIND']])
    print('submitted: ' + task_name)
